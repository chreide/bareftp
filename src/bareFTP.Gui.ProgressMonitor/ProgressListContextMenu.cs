// ProgressListContextMenu.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using bareFTP.Protocol;
using Mono.Unix;

namespace bareFTP.Gui.ProgressMonitor
{
	public class ProgressListContextMenu : Gtk.Menu
	{
		public event EventHandler SkipRequested;
		public event EventHandler AbortRequested;
		public event EventHandler MoveUpRequested;
		public event EventHandler MoveDownRequested;
		
		public ProgressListContextMenu(XferFile file, XferFile prevfile, XferFile nextfile) : base()
		{
			if(file.Status == DownloadStatus.Queued)
			{
				Gtk.ImageMenuItem m1 = new Gtk.ImageMenuItem(Catalog.GetString("Skip") + "..");
				m1.Activated += delegate (object sender, EventArgs e) { SkipRequested ( this, e ); };
				
				if(Gtk.IconTheme.Default.HasIcon(Gtk.Stock.Close))
					m1.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.Close, 16, 0));
				
				this.Append(m1);
				Gtk.ImageMenuItem m3 = new Gtk.ImageMenuItem (Catalog.GetString("Move up") + "..");
				m3.Activated += delegate (object sender, EventArgs e) { MoveUpRequested ( this, e ); };
				if(Gtk.IconTheme.Default.HasIcon(Gtk.Stock.GoUp))
					m3.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.GoUp, 16, 0));
				else
					m3.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon("stock_up", 16, 0));
				if(prevfile != null && prevfile.Status == DownloadStatus.Queued)
					m3.Sensitive = true;
				else
					m3.Sensitive = false;
				this.Append(m3);
				Gtk.ImageMenuItem m4 = new Gtk.ImageMenuItem (Catalog.GetString("Move down") + "..");
				m4.Activated += delegate (object sender, EventArgs e) { MoveDownRequested ( this, e ); };
				if(Gtk.IconTheme.Default.HasIcon(Gtk.Stock.GoUp))
					m4.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.GoDown, 16, 0));
				else
					m4.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon("stock_down", 16, 0));
				if(nextfile != null && nextfile.Status == DownloadStatus.Queued)
					m4.Sensitive = true;
				else
					m4.Sensitive = false;
				this.Append(m4);
			}
			else if(file.Status == DownloadStatus.Downloading || (file.Status == DownloadStatus.Uploading))
			{
				Gtk.ImageMenuItem m2 = new Gtk.ImageMenuItem (Catalog.GetString("Abort") + "..");
				m2.Activated += delegate (object sender, EventArgs e) { AbortRequested ( this, e ); };
				if(Gtk.IconTheme.Default.HasIcon(Gtk.Stock.Cancel))
					m2.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.Cancel, 16, 0));
				this.Append(m2);
				m2.Sensitive = true;
			}
			
			
		}			
	}
}
