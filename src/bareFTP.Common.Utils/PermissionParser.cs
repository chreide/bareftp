// FileCopier.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.Generic;

namespace bareFTP.Common.Utils
{
	
	
	public class PermissionParser
	{
		
		
		public static string UFAPToMode(Mono.Unix.FileAccessPermissions fap)
		{
			int x = (int)fap;
			int u = (x & 448) >> 6;
			int g = (x & 56) >> 3;
			int o = (x & 7);

			return string.Format("{0}{1}{2}",u,g,o);
		}

		public static string ModeToRWXT(string permissions)
		{
			string str = string.Empty;
			if(permissions.Length == 4)
				permissions = permissions.Substring(1,3);
			
			foreach(char c in permissions.ToCharArray())
			{
				int x = Int32.Parse(c.ToString());

				if((x & 4) == 4)
					str += "r";
				else
					str += "-";
				if((x & 2) == 2)
					str += "w";
				else
					str += "-";
				if((x & 1) == 1)
					str += "x";
				else
					str += "-";
			}

			return str;
		}
		
		public static string UFAPToRWX(Mono.Unix.FileAccessPermissions fap)
		{
			string str = string.Empty;
			
			if((fap & Mono.Unix.FileAccessPermissions.UserReadWriteExecute) == Mono.Unix.FileAccessPermissions.UserReadWriteExecute)
			{
				str = "rwx";
			}
			else
			{
				if((fap & Mono.Unix.FileAccessPermissions.UserRead) == Mono.Unix.FileAccessPermissions.UserRead)
					str += "r";
				else
					str += "-";
				if((fap & Mono.Unix.FileAccessPermissions.UserWrite) == Mono.Unix.FileAccessPermissions.UserWrite)
					str += "w";
				else
					str += "-";
				if((fap & Mono.Unix.FileAccessPermissions.UserExecute) == Mono.Unix.FileAccessPermissions.UserExecute)
					str += 'x';
				else
					str += "-";
			}
			if((fap & Mono.Unix.FileAccessPermissions.GroupReadWriteExecute) == Mono.Unix.FileAccessPermissions.GroupReadWriteExecute)
			{
				str += "rwx";
			}
			else
			{
				
				if((fap & Mono.Unix.FileAccessPermissions.GroupRead) == Mono.Unix.FileAccessPermissions.GroupRead)
					str += 'r';
				else
					str += "-";
				if((fap & Mono.Unix.FileAccessPermissions.GroupWrite) == Mono.Unix.FileAccessPermissions.GroupWrite)
					str += 'w';
				else
					str += "-";
				if((fap & Mono.Unix.FileAccessPermissions.GroupExecute) == Mono.Unix.FileAccessPermissions.GroupExecute)
					str += 'x';
				else
					str += "-";
			}
			if((fap & Mono.Unix.FileAccessPermissions.OtherReadWriteExecute) == Mono.Unix.FileAccessPermissions.OtherReadWriteExecute)
			{
				str += "rwx";
			}
			else
			{
				if((fap & Mono.Unix.FileAccessPermissions.OtherRead) == Mono.Unix.FileAccessPermissions.OtherRead)
					str += 'r';
				else
					str += "-";
				if((fap & Mono.Unix.FileAccessPermissions.OtherWrite) == Mono.Unix.FileAccessPermissions.OtherWrite)
					str += 'w';
				else
					str += "-";
				if((fap & Mono.Unix.FileAccessPermissions.OtherExecute) == Mono.Unix.FileAccessPermissions.OtherExecute)
					str += 'x';
				else
					str += "-";
			}			
		
			return str;
		}

		public static string RWXToMode(string permissions)
		{
			permissions = permissions.Remove(0,1);
			byte u = 0;
			byte g = 0;
			byte o = 0;
			
			string us = permissions.Substring(0,3);
			string gs = permissions.Substring(3,3);
			string os = permissions.Substring(6,3);
		
			if(us.IndexOf("r") >= 0)
				u = Convert.ToByte(u | Convert.ToByte(4));
			if(us.IndexOf("w") >= 0)
				u = Convert.ToByte(u | Convert.ToByte(2));
			if(us.IndexOf("x") >= 0)
				u = Convert.ToByte(u | Convert.ToByte(1));
			
			if(gs.IndexOf("r") >= 0)
				g = Convert.ToByte(g | Convert.ToByte(4));
			if(gs.IndexOf("w") >= 0)
				g = Convert.ToByte(g | Convert.ToByte(2));
			if(gs.IndexOf("x") >= 0)
				g = Convert.ToByte(g | Convert.ToByte(1));
			
			if(os.IndexOf("r") >= 0)
				o = Convert.ToByte(o | Convert.ToByte(4));
			if(os.IndexOf("w") >= 0)
				o = Convert.ToByte(o | Convert.ToByte(2));
			if(os.IndexOf("x") >= 0)
				o = Convert.ToByte(o | Convert.ToByte(1));
			
			return Convert.ToInt32(u).ToString() + Convert.ToInt32(g).ToString() + Convert.ToInt32(o).ToString();
		
		}

		

	}
}
