// AskFileAction.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using System.Collections.Generic;
using bareFTP.Protocol;

namespace bareFTP.Gui.Dialog
{
	
	
	public partial class AskFileAction : Gtk.Dialog
	{
		
		private FileActionContextMenu menu;
		private bareNodeView nodeview1;
		
		public AskFileAction(List<bareFTP.Protocol.XferFile> files)
		{
			this.Build();
			
			this.combobox1.AppendText(Mono.Unix.Catalog.GetString("Overwrite"));
            this.combobox1.AppendText(Mono.Unix.Catalog.GetString("Resume"));
            this.combobox1.AppendText(Mono.Unix.Catalog.GetString("Append"));
            this.combobox1.AppendText(Mono.Unix.Catalog.GetString("Skip"));
			this.combobox1.Active = 0;
			menu = new FileActionContextMenu();
			menu.ActionChanged += new ActionChangedHandler(OnActionChanged);
			
			nodeview1 = new bareNodeView(); 
			nodeview1.AppendColumn(Mono.Unix.Catalog.GetString("File"),   new MyCellRendererText(120), "text", 0);
			nodeview1.AppendColumn(Mono.Unix.Catalog.GetString("Local size"),   new MyCellRendererText(80), "text", 1);
			nodeview1.AppendColumn(Mono.Unix.Catalog.GetString("Remote size"), new MyCellRendererText(80), "text", 2);
			nodeview1.AppendColumn(Mono.Unix.Catalog.GetString("Action"), new MyCellRendererText(80), "text", 3);
			
			nodeview1.MouseRightClicked += new EventHandler(testevt);
			
			Gtk.NodeStore store = new Gtk.NodeStore(typeof(FileActionNode));
			if(files.Count > 0)
			{			
				foreach(XferFile file in files)
				{
					if(!file.IsDir && file.Marker > 0)
					{
						//TODO: get default from config when we have it
						file.Action = FileAction.Overwrite;
						store.AddNode(new FileActionNode(file));
					}
				}
			}
			
			nodeview1.NodeStore = store;			
			this.scrolledwindow1.Add(nodeview1);
			nodeview1.ShowAll();
			buttonOk.GrabFocus();
			
		}
		
		protected void OnActionChanged(FileActionArgs args)
		{
			if(nodeview1.NodeSelection.SelectedNodes.Length >= 1)
			{	
    			foreach(Gtk.ITreeNode inode in nodeview1.NodeSelection.SelectedNodes)
    			{
    				FileActionNode node = inode as FileActionNode;
    				node.File.Action = args.Action;
    			}
    		}
		}
		
		protected void testevt(object obj, EventArgs arg)
		{
   				menu.Popup();
   				menu.ShowAll();				
		}

		protected virtual void SetForAllClicked (object sender, System.EventArgs e)
		{
			foreach(FileActionNode node in nodeview1.NodeStore)
			{
				if(combobox1.Active == 0)
					node.File.Action = FileAction.Overwrite;
				else if(combobox1.Active == 1)
					node.File.Action = FileAction.Resume;
				else if(combobox1.Active == 2)
					node.File.Action = FileAction.Append;
				else if(combobox1.Active == 3)
					node.File.Action = FileAction.Skip;
				
			}
			nodeview1.QueueDraw();
		}
		
	}
	
	public delegate void ActionChangedHandler (FileActionArgs args);
	
	public class FileActionContextMenu : Gtk.Menu
	{
		
		private Gtk.MenuItem overwrite;
		private Gtk.MenuItem resume;
		private Gtk.MenuItem append;
		private Gtk.MenuItem skip;
		
		public event ActionChangedHandler ActionChanged;
		
		public FileActionContextMenu() : base()
		{
			overwrite = new Gtk.MenuItem(Mono.Unix.Catalog.GetString("Overwrite"));
			overwrite.Activated += new EventHandler(OverwriteSelected);
			this.Append (overwrite);
			
			resume = new Gtk.MenuItem(Mono.Unix.Catalog.GetString("Resume"));
			resume.Activated += new EventHandler(ResumeSelected);
			this.Append (resume);
			
			append = new Gtk.MenuItem(Mono.Unix.Catalog.GetString("Append"));
			append.Activated += new EventHandler(AppendSelected);
			this.Append (append);
			
			skip = new Gtk.MenuItem(Mono.Unix.Catalog.GetString("Skip"));
			skip.Activated += new EventHandler(SkipSelected);
			this.Append (skip);
			
			this.ShowAll();
		}
		
		private void OverwriteSelected(object o, EventArgs e)
		{
			ActionChanged(new FileActionArgs(FileAction.Overwrite));
		}
		private void AppendSelected(object o, EventArgs e)
		{
			ActionChanged(new FileActionArgs(FileAction.Append));
		}
		private void ResumeSelected(object o, EventArgs e)
		{
			ActionChanged(new FileActionArgs(FileAction.Resume));
		}
		private void SkipSelected(object o, EventArgs e)
		{
			ActionChanged(new FileActionArgs(FileAction.Skip));
		}
	}
	
	public class FileActionArgs : EventArgs
	{
		private bareFTP.Protocol.FileAction action;
		
		public FileActionArgs(FileAction action)
		{
			this.action = action;
		}
		
		public FileAction Action
		{
			get { return action; }
		}
	}
}
