
using System;

namespace bareFTP.Gui.Dialog
{
	
	
	public partial class SearchDialog : Gtk.Dialog
	{
		
		public string SearchTerm {
			get {
				return entry1.Text;
			}
		}
		
		public SearchDialog()
		{
			this.Build();
		}
	}
}
