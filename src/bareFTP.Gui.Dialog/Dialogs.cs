// Dialogs.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using Gtk;

namespace bareFTP.Gui.Dialog
{
	
	
	public class Dialogs
	{
		public static void ErrorDialog(string msg)
		{
			MessageDialog md = new MessageDialog(null, DialogFlags.DestroyWithParent, 
				                                      MessageType.Error, ButtonsType.Ok,
				                                      msg); 
				md.Run ();
				md.Destroy();
			
		}
		
		public static bool QuestionDialog(string msg)
		{
			
			MessageDialog md = new MessageDialog(null, DialogFlags.DestroyWithParent, 
				                                      MessageType.Question, ButtonsType.YesNo,
				                                      msg);
			
			ResponseType result = (ResponseType)md.Run ();
			md.Destroy();
			
			return result == ResponseType.Yes ? true : false;
		}

	}
}
