// BookmarkEntry.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;

namespace bareFTP.Preferences.Bookmarks
{
	
	public class BookmarkEntry : BookmarkItem
	{
		string host = string.Empty;
		string port = string.Empty;
		string user = string.Empty;
		string pass = string.Empty;
		int protocol;
		bool showhidden;
		bool passive;
		bool sync_browse;
		string encryptdata = string.Empty;
		string remotepath = string.Empty;
		string localpath = string.Empty;
		string charset = string.Empty;
		int keyId = -1;
		
		public BookmarkEntry(string name) : base(name)
		{
			Setup();
		}
		
		private void Setup()
		{
			bareFTP.Preferences.Config conf = new bareFTP.Preferences.Config(string.Empty);
			this.passive = conf.FTP_PassiveMode;
			this.showhidden = conf.General_ShowHiddenFiles;
			this.encryptdata = conf.FTPSDataChannelProtectionLevel;
			this.protocol = conf.NetworkDefaultProtocol;
		}
		
		public string User {
			get {
				return user;
			}
			set {
				user = value;
			}
		}
		
		public string Port {
			get {
				return port;
			}
			set {
				port = value;
			}
		}
		
		public string Pass {
			get {
				return pass;
			}
			set {
				pass = value;
			}
		}
		
		public string Host {
			get {
				return host;
			}
			set {
				host = value;
			}
		}

		public int Protocol {
			get {
				return protocol;
			}
			set {
				protocol = value;
			}
		}

		public bool ShowHidden {
			get {
				return showhidden;
			}
			set {
				showhidden = value;
			}
		}

		public string CharSet {
			get {
				return charset;
			}
			set {
				charset = value;
			}
		}

		public string RemotePath {
			get {
				return remotepath;
			}
			set {
				remotepath = value;
			}
		}

		public string LocalPath {
			get {
				return localpath;
			}
			set {
				localpath = value;
			}
		}

		public bool Passive {
			get {
				return passive;
			}
			set {
				passive = value;
			}
		}
		
		public bool SyncedBrowse {
			get {
				return sync_browse;
			}
			set {
				sync_browse = value;
			}
		}

		public string EncryptData {
			get {
				return encryptdata;
			}
			set {
				encryptdata = value;
			}
		}
				
		public int KeyId
		{
			get { return keyId; }
			set { keyId = value; }
		}
		
	}
}
