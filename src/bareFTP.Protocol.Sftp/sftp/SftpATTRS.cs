using System;
using System.Text;
using System.Collections.Generic;

namespace bareFTP.Protocol.Sftp
{
	
	/* This file is slightly modified by bareFTP author to better 
	 * suit bareFTP. Original copyright notice follows...
	 */
	/* ============================================================== */
	
	/*
	Copyright (c) 2002,2003,2004 ymnk, JCraft,Inc. All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	  1. Redistributions of source code must retain the above copyright notice,
		 this list of conditions and the following disclaimer.

	  2. Redistributions in binary form must reproduce the above copyright 
		 notice, this list of conditions and the following disclaimer in 
		 the documentation and/or other materials provided with the distribution.

	  3. The names of the authors may not be used to endorse or promote products
		 derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL JCRAFT,
	INC. OR ANY CONTRIBUTORS TO THIS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
	LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
	OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
	EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/

	/*
	  uint32   flags
	  uint64   size           present only if flag SSH_FILEXFER_ATTR_SIZE
	  uint32   uid            present only if flag SSH_FILEXFER_ATTR_UIDGID
	  uint32   gid            present only if flag SSH_FILEXFER_ATTR_UIDGID
	  uint32   permissions    present only if flag SSH_FILEXFER_ATTR_PERMISSIONS
	  uint32   atime          present only if flag SSH_FILEXFER_ACMODTIME
	  uint32   mtime          present only if flag SSH_FILEXFER_ACMODTIME
	  uint32   extended_count present only if flag SSH_FILEXFER_ATTR_EXTENDED
	  string   extended_type
	  string   extended_data
		...      more extended data (extended_type - extended_data pairs),
				 so that number of pairs equals extended_count
	*/
	public class SftpATTRS 
	{

		static  int S_ISUID = Convert.ToInt32("04000",8); // set user ID on execution
		static  int S_ISGID = Convert.ToInt32("02000",8); // set group ID on execution
		//static  int S_ISVTX = 01000; // sticky bit   ****** NOT DOCUMENTED *****

		static  int S_IRUSR = Convert.ToInt32("00400",8); // read by owner
		static  int S_IWUSR = Convert.ToInt32("00200",8); // write by owner
		static  int S_IXUSR = Convert.ToInt32("00100",8); // execute/search by owner
		//static  int S_IREAD = 00400; // read by owner
		//static  int S_IWRITE= 00200; // write by owner
		//static  int S_IEXEC = 00100; // execute/search by owner

		static  int S_IRGRP = Convert.ToInt32("00040",8); // read by group
		static  int S_IWGRP = Convert.ToInt32("00020",8); // write by group
		static  int S_IXGRP = Convert.ToInt32("00010",8); // execute/search by group

		static  int S_IROTH = Convert.ToInt32("00004",8); // read by others
		static  int S_IWOTH = Convert.ToInt32("00002",8); // write by others
		static  int S_IXOTH = Convert.ToInt32("00001",8); // execute/search by others

		private static int pmask = 0xFFF;

		public string getPermissionsString() 
		{
			StringBuilder buf = new StringBuilder(10);

			if(isDir()) buf.Append('d');
			else if(isLink()) buf.Append('l');
			else buf.Append('-');

			if((permissions & S_IRUSR)!=0) buf.Append('r');
			else buf.Append('-');

			if((permissions & S_IWUSR)!=0) buf.Append('w');
			else buf.Append('-');

			if((permissions & S_ISUID)!=0) buf.Append('s');
			else if ((permissions & S_IXUSR)!=0) buf.Append('x');
			else buf.Append('-');

			if((permissions & S_IRGRP)!=0) buf.Append('r');
			else buf.Append('-');

			if((permissions & S_IWGRP)!=0) buf.Append('w');
			else buf.Append('-');

			if((permissions & S_ISGID)!=0)
				buf.Append('s');
			
			else if((permissions & S_IXGRP)!=0) 
				buf.Append('x');
				
			else 
				buf.Append('-');

			if((permissions & S_IROTH) != 0) buf.Append('r');
			else buf.Append('-');

			if((permissions & S_IWOTH) != 0) buf.Append('w');
			else buf.Append('-');

			if((permissions & S_IXOTH) != 0) buf.Append('x');
			else buf.Append('-');
			return (buf.ToString());
		}

		public string  getAtimeString()
		{
			//SimpleDateFormat locale=new SimpleDateFormat();
			//return (locale.format(new Date(atime)));
			//[tamir] use Time_T2DateTime to convert t_time to DateTime
			DateTime d = Util.Time_T2DateTime((uint)atime);
			return d.ToShortDateString();
			
		}

		public DateTime Mtime
		{
			//[tamir] use Time_T2DateTime to convert t_time to DateTime
			get{ return Util.Time_T2DateTime((uint)mtime); }		
		}

		public static  int SSH_FILEXFER_ATTR_SIZE=         0x00000001;
		public static  int SSH_FILEXFER_ATTR_UIDGID=       0x00000002;
		public static  int SSH_FILEXFER_ATTR_PERMISSIONS=  0x00000004;
		public static  int SSH_FILEXFER_ATTR_ACMODTIME=    0x00000008;
		public static  uint SSH_FILEXFER_ATTR_EXTENDED=    0x80000000;

		static  int S_IFDIR=0x4000;
		static  int S_IFLNK=0xa000;
				
		int flags= 0;
		long size;
		internal int uid;
		internal int gid;
		int permissions;
		int atime;
		int mtime;
		List<string> extended = null;

		private SftpATTRS()
		{
		}

		internal static SftpATTRS getATTR(Buffer buf)
		{
			SftpATTRS attr=new SftpATTRS();	
			attr.flags = buf.getInt();
			if((attr.flags & SSH_FILEXFER_ATTR_SIZE) != 0){ attr.size = buf.getLong(); }
			if((attr.flags & SSH_FILEXFER_ATTR_UIDGID) != 0)
			{
				attr.uid=buf.getInt(); attr.gid=buf.getInt();
			}
			if((attr.flags & SSH_FILEXFER_ATTR_PERMISSIONS) != 0)
			{ 
				attr.permissions=buf.getInt();
			}
			if((attr.flags & SSH_FILEXFER_ATTR_ACMODTIME) != 0)
			{ 
				attr.atime=buf.getInt();
			}
			if((attr.flags & SSH_FILEXFER_ATTR_ACMODTIME) != 0)
			{ 
				attr.mtime=buf.getInt(); 
			}
			if((attr.flags & SSH_FILEXFER_ATTR_EXTENDED) != 0)
			{
				int count=buf.getInt();
				if(count > 0)
				{
					attr.extended=new List<string>();
					for(int i=0; i<count; i++)
					{
						attr.extended.Add(Util.getString(buf.getString()));
						attr.extended.Add(Util.getString(buf.getString()));
					}
				}
			}
			return attr;
		} 

		internal int Length
		{
			get { return length(); }
		}

		internal int length()
		{
			int len=4;

			if((flags&SSH_FILEXFER_ATTR_SIZE)!=0){ len+=8; }
			if((flags&SSH_FILEXFER_ATTR_UIDGID)!=0){ len+=8; }
			if((flags&SSH_FILEXFER_ATTR_PERMISSIONS)!=0){ len+=4; }
			if((flags&SSH_FILEXFER_ATTR_ACMODTIME)!=0){ len+=8; }
			if((flags&SSH_FILEXFER_ATTR_EXTENDED)!=0)
				{
				len+=4;
				int count=extended.Count/2;
				if(count>0)
				{
					foreach(string s in extended)
					{
						len +=4;
						len += s.Length;
					}
				}
			}
			return len;
		}

		internal void dump(Buffer buf)
		{
			buf.putInt(flags);
			if((flags&SSH_FILEXFER_ATTR_SIZE)!=0){ buf.putLong(size); }
			if((flags&SSH_FILEXFER_ATTR_UIDGID)!=0)
			{
				buf.putInt(uid); buf.putInt(gid);
			}
			if((flags&SSH_FILEXFER_ATTR_PERMISSIONS)!=0)
			{ 
				buf.putInt(permissions);
			}
			if((flags&SSH_FILEXFER_ATTR_ACMODTIME)!=0){ buf.putInt(atime); }
			if((flags&SSH_FILEXFER_ATTR_ACMODTIME)!=0){ buf.putInt(mtime); }
			if((flags&SSH_FILEXFER_ATTR_EXTENDED)!=0)
			{
				int count=extended.Count/2;
				if(count>0)
				{
					for(int i=0; i<count; i++)
					{
						buf.putString(Util.getBytes(extended[i*2]));
						buf.putString(Util.getBytes(extended[i*2+1]));
					}
				}
			}
		}
		
		internal void setFLAGS(int flags)
		{
			this.flags=flags;
		}
		
		public void setSIZE(long size)
		{
			flags|=SSH_FILEXFER_ATTR_SIZE;
			this.size=size;
		}
		
		public void setUIDGID(int uid, int gid)
		{
			flags|=SSH_FILEXFER_ATTR_UIDGID;
			this.uid=uid;
			this.gid=gid;
		}
		
		public void setACMODTIME(int atime, int mtime)
		{
			flags|=SSH_FILEXFER_ATTR_ACMODTIME;
			this.atime=atime;
			this.mtime=mtime;
		}
		
		public void setPERMISSIONS(int permissions)
		{
			flags|=SSH_FILEXFER_ATTR_PERMISSIONS;
			permissions=(this.permissions&~pmask)|(permissions&pmask);
			this.permissions=permissions;
		}

		public bool isDir()
		{
			return ((flags&SSH_FILEXFER_ATTR_PERMISSIONS)!=0 && 
				((permissions&S_IFDIR)==S_IFDIR));
		}      
		
		public bool isLink()
		{
			return ((flags&SSH_FILEXFER_ATTR_PERMISSIONS)!=0 && 
				((permissions&S_IFLNK)==S_IFLNK));
		}
		
		public int Flags { get { return flags; }}
		public long Size { get {return size; }}
		public int UId { get { return uid; }}
		public int GId { get { return gid; }}
		public int Permissions { get { return permissions; }}
		public int ATime { get { return atime; }}
		public int MTime { get { return mtime; }}
		public List<string> Extended { get { return extended; }}

	}

}
