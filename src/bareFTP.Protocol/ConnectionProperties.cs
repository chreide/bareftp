// ConnectionProperties.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;

namespace bareFTP.Protocol
{
	
	
	public class ConnectionProperties
	{
		string hostname;
		int port = 0;
		string user;
		string password;
		bool ftps = false;
		int protocol = 0;
		bool passive = true;
		string charset = string.Empty;
		
		bareFTP.Preferences.Bookmarks.BookmarkEntry bookmark = null;
		
		public string Hostname {
			get {
				return hostname;
			}
			set {
				hostname = value;
			}
		}

		public int Port {
			get {
				return port;
			}
			set {
				port = value;
			}
		}

		public string User {
			get {
				return user;
			}
			set {
				user = value;
			}
		}

		public string Password {
			get {
				return password;
			}
			set {
				password = value;
			}
		}

		public bool FTPS {
			get {
				return ftps;
			}
			set {
				ftps = value;
			}
		}
		
		public string RemoteCharset {
			get {
				return charset;
			}
			set {
				charset = value;
			}
		}

		public bareFTP.Preferences.Bookmarks.BookmarkEntry Bookmark
		{
			get { return bookmark; }
			set { bookmark = value; }
		}

		public int Protocol {
			get {
				return protocol;
			}
			set {
				protocol = value;
			}
		}

		public bool Passive {
			get {
				return passive;
			}
			set {
				passive = value;
			}
		}
		
	}
}
