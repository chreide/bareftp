// IProtocol.cs
//
//  Copyright (C) 2008 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using System.Collections.Generic;
using System.IO;

namespace bareFTP.Protocol
{
	
	public delegate void DisplayDialogHandler (EventArgs args);
	
	public interface IProtocol
	{		
		
		event EventHandler LogTextEmitted;
		event EventHandler LostConnection;
		
		ProtocolType ProtocolType { get; }
		
		void Init(IDialogHost host, bareFTP.Preferences.Config conf);
		
		void Open(string remoteHost, int remotePort, string user, string password);
		void Close();
		bool Connected { get; }
		bool TransferInProgress { get; set;}
		
		//TODO:  For sending general commands through the command window. Returns result. (maybe a list is better?)
		void SendCommand(string command);
		// Server output
		//List<string> GetMessageList();
		
		// Directory listings
		List<string> Dir();
		List<RemoteFile> XDir();
		
		// File transfers
		void SendFile(XferFile file);
		void GetFile(XferFile file);
		
		// File operations
		void DeleteFile(String remoteFileName);
		void MoveFile(string remoteFileName, string toRemotePath);
		void RenameFile(string fromRemoteFileName, string toRemoteFileName);
		void SetCurrentDirectory(String remotePath);
		string GetCurrentDirectory();
		void MakeDir(string directoryName);
		void RemoveDir(string directoryName);
		void Chmod(string mode, string filename);
		void Abort();
		bool IsDir(string cwd, string dir);
		// The following property will NOT pwd to server. Get pwd from connection object.
		string CurrentDirectory { get; }
	}
	
	public enum MessageType : int
	{
		Undef = 0,
		Info = 1,
		Error = 2,
		Debug = 3,
		Welcome = 4,
		ClientCommand = 5,
		ClientError = 6
	}
	
	public class LogTextEmittedArgs : EventArgs
	{
		private List<FTPReply> reply;

		public LogTextEmittedArgs(int code, string message)
		{
			reply = new List<FTPReply>();
			reply.Add(new FTPReply(code, message));
		}
		public LogTextEmittedArgs(List<FTPReply> reply)
		{
			this.reply = reply;
		}
		public LogTextEmittedArgs(MessageType type, string message)
		{
			reply = new List<FTPReply>();
			reply.Add(new FTPReply(type, message));
		}
		
		public List<FTPReply> Reply
		{
			get {
				return reply;
			}
		}
	}
	
	public class TransferProgressEmittedArgs : EventArgs
	{
		public TransferProgressEmittedArgs(object e)
		{
			fileTransferInfo = e;
		}
		private object fileTransferInfo;
		
		public object FileTransferInfo 
		{
			get { return fileTransferInfo; }
			set { fileTransferInfo = value; }
		}
	}
	
}
