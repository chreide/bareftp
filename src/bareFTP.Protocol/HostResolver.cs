// HostResolver.cs
//
//  Copyright (C) 2010 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using System.Net;
using System.Net.Sockets;
using System.Globalization;

namespace bareFTP.Protocol
{
	public class HostResolver
    {
        public static IPAddress GetAddress(string hostName)
        {
			IdnMapping idn = new IdnMapping();
			
			IPAddress[] addresses = Dns.GetHostAddresses(idn.GetAscii(hostName));
			
			foreach (IPAddress a in addresses)
            	if (a.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
					return a;
			
            throw new ArgumentException(hostName + " resolves to an unsupported protocol.");
			
        }
    }
}
