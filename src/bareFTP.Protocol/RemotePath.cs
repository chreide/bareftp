// RemotePath.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;

namespace bareFTP.Protocol
{
	
	
	public class BarePath
	{
		bool vms = false;
		char separator = '/';
		string visualpathremote = string.Empty;
		string rootpathremote = string.Empty;
		string rootpathlocal = string.Empty;
		string filename = string.Empty;
		string relativepath = string.Empty;
		
		
		public string AddElementRemote(string element)
		{
			if(!string.IsNullOrEmpty(element))
				rootpathremote = rootpathremote + separator + element;
			return rootpathremote;
		}

		public string AddElementLocal(string element)
		{
			if(!string.IsNullOrEmpty(element))
				rootpathlocal = rootpathlocal + System.IO.Path.DirectorySeparatorChar + element;
			return rootpathlocal;
		}

		public string RemoveElementRemote()
		{
			if(rootpathremote.LastIndexOf(separator) < 0)
				rootpathremote = string.Empty;
			else
				rootpathremote = rootpathremote.Remove(rootpathremote.LastIndexOf(separator));
			
			if(string.IsNullOrEmpty(rootpathremote))
			{
				if(vms)
					rootpathremote = "[]";
				else
					rootpathremote = "/";
			}
			return RemotePath;
		}

		public string RemoveElementLocal()
		{
			rootpathlocal = rootpathlocal.Remove(rootpathlocal.LastIndexOf(System.IO.Path.DirectorySeparatorChar));
			if(string.IsNullOrEmpty(rootpathlocal))
				return System.IO.Path.DirectorySeparatorChar.ToString();
			else
				return rootpathlocal;
		}

		public void SetBaseRemote(string path)
		{
			if(string.IsNullOrEmpty(path) || path.IndexOf('/') >= 0)
			{
				vms = false;
				this.rootpathremote = path;
			}
			else
			{
				vms = true;
				separator = '.';
				visualpathremote = path.Substring(0, path.IndexOf(':')+1);
				int start = path.IndexOf('[');
				int end = path.IndexOf(']');
				this.rootpathremote = path.Substring(start+1, end - start -1);
			}
		}

		public void SetBaseLocal(string path)
		{
			this.rootpathlocal = path;
		}

		public void AddRelativeElement(string dir)
		{
			relativepath = relativepath + separator + dir;
		}
		public void SetFileName(string dir, string file)
		{
			relativepath = dir;
			filename = file;
		}

		public bool VMS {
			get {
				return vms;
			}
		}

		public string RemotePath
		{
			get 
			{
				if(!vms)
					return rootpathremote;
				else
					return "[" + rootpathremote + "]";
			}
		}

		public string LocalPath
		{
			get 
			{
				return rootpathlocal;
			}
		}

		public string FileName
		{
			get { return filename; }
		}

		public string FileNameRemoteAbs
		{
			get 
			{
				
				if(vms)
				{
					if(!string.IsNullOrEmpty(relativepath))
						return "[" + rootpathremote + separator + relativepath + "]" + filename;
					else
						return  RemotePath + filename;
				}
				else
				{
					string path = RemotePath;
					if(!path.EndsWith(separator.ToString()))
						path += separator;
					
					if(!string.IsNullOrEmpty(relativepath))
					{
						path = path + RelativePathRemote;
						if(!path.EndsWith(separator.ToString()))
							path += separator;
					
					}
					return path + filename;
				}
			}
		}

		public string RemotePathAbs
		{
			get 
			{
				
				if(vms)
				{
					if(!string.IsNullOrEmpty(relativepath))
						return "[" + rootpathremote + separator + relativepath + "]";
					else
						return  RemotePath;
				}
				else
				{
					if(!string.IsNullOrEmpty(relativepath))
						return (RemotePath + separator + RelativePathRemote).Replace("//","/");
					else
						return RemotePath;
				}
			}
		}

		public string FileNameLocalAbs
		{
			get 
			{
				string path = rootpathlocal;
				if(!string.IsNullOrEmpty(relativepath))
					path = System.IO.Path.Combine(rootpathlocal, RelativePathLocal);

				return System.IO.Path.Combine(path, FileName);
			}
		}

		public string RelativePathRemote
		{
			get 
			{
				return relativepath.Replace(System.IO.Path.DirectorySeparatorChar, separator).TrimStart(separator);
			}
		}

		public string RelativePathLocal
		{
			get 
			{ 
				return relativepath.Replace(separator, System.IO.Path.DirectorySeparatorChar).TrimStart(System.IO.Path.DirectorySeparatorChar);;
			}
		}
		
		public string FileNameDisplay
		{
			get
			{
				return System.IO.Path.Combine(RelativePathLocal + "/", FileName).TrimStart('/');
			}
		}

		public char Separator
		{
			get { return separator; }
		}

		public string ToString(bool remote)
		{
			if(remote)
			{
				if(!VMS)
					return rootpathremote;
				else
					return visualpathremote + "[" + rootpathremote + "]";
			}
			else
				return rootpathlocal;
		}

	}
}
