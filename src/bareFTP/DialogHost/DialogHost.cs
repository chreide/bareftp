// DialogHost.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using Gtk;

namespace bareFTP
{
	
	
	public class ProtocolHost : bareFTP.Protocol.IDialogHost
	{
		
		public ProtocolHost()
		{
		
		}
				
		public bool DisplayDialogQuestion(System.Threading.EventWaitHandle ewh, string msg)
		{
			bool result = bareFTP.Gui.Dialog.Dialogs.QuestionDialog(msg);
			
			if(ewh != null)
				ewh.Set();
			return result;
		}
		
		public void DisplayDialogError(System.Threading.EventWaitHandle ewh, string msg)
		{
			bareFTP.Gui.Dialog.Dialogs.ErrorDialog(msg);
			if(ewh != null)
				ewh.Set();
		}
		
		public void DisplayDialogInfo(System.Threading.EventWaitHandle ewh, string msg)
		{
			
			MessageDialog md = new MessageDialog(null, DialogFlags.DestroyWithParent, 
				                                      MessageType.Info, ButtonsType.Ok, 
				                                      msg);
			md.Run ();
			md.Destroy();
			if(ewh != null)
				ewh.Set();
		}
		
		public void DisplayDialogWarning(System.Threading.EventWaitHandle ewh, string msg)
		{
			MessageDialog md = new MessageDialog(null, DialogFlags.DestroyWithParent, 
				                                      MessageType.Warning, ButtonsType.Ok, 
				                                      msg);
			md.Run ();
			md.Destroy();
			if(ewh != null)
				ewh.Set();
		}
		
		public string DisplayPasswordDialog(System.Threading.EventWaitHandle ewh)
		{
			string pwd = string.Empty;
			bareFTP.Gui.Dialog.AskPasswordDialog apd = new bareFTP.Gui.Dialog.AskPasswordDialog();
			apd.Run();
			pwd = apd.Password;
			apd.Destroy();
			if(ewh != null)
				ewh.Set();
			return pwd;
		}
	}
}
