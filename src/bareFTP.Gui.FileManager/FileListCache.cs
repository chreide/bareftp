// FileListCache.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

// TODO: Make some kind of periodical cleaning of cache

namespace bareFTP.Gui.FileManager
{
	public class FileListCache
    {
    	private int timeout;
    	private List<CacheEntry> cache;
    	private Dictionary<string, Gtk.TreePath> treepaths;
		
    	public FileListCache(int cache_timeout)
    	{
    		this.timeout = cache_timeout;
    		cache = new List<CacheEntry>();
			treepaths = new Dictionary<string, Gtk.TreePath>();
    	}
    	
    	public Gtk.ListStore GetCachedStore(string directory)
    	{
			try
			{
	    		for(int x=0;x<cache.Count;x++)
	    		{	
	    			if(directory == cache[x].Directory)
	    			{
	    				if(cache[x].Timestamp.AddSeconds(timeout) > DateTime.Now)
	    					return cache[x].Store;
	    				else
						{
	    					cache.RemoveAt(x);
	    					return null;
	    				}
	    			}
	    		}
	    		return null;
			}
			catch { return null; }
    	}
		
		public void RemoveEntry(string directory)
		{
			try
			{
				List<CacheEntry> obsolteentries = new List<CacheEntry>();
				
				for(int x=0;x<cache.Count;x++)
	    		{	
	    			if(directory == cache[x].Directory || cache[x].Timestamp.AddSeconds(timeout) > DateTime.Now)
	    			{
						obsolteentries.Add(cache[x]);
	    			}
	    		}
				
				foreach(CacheEntry obsoleteenttry in obsolteentries)
					cache.Remove(obsoleteenttry);
			}
			catch {}
		}
    	
    	public void AddEntry(string directory, Gtk.ListStore store)
    	{
    		CacheEntry entry = new CacheEntry(directory, store);
    		bool haveold = false;
    		
    		for(int x=0;x<cache.Count;x++)
    		{
    			if(entry.Directory == cache[x].Directory)
    			{
    				cache[x] = entry;
    				haveold = true;
    				break;
    			}
    		}
    		if(!haveold)
    			cache.Add(entry);
    		
    	}
		
		public void AddCachedTreePath(string directory, Gtk.TreePath treepath)
		{
			int depth = Regex.Matches(directory, "/").Count;
			List<string> obsoletepaths = new List<string>();
			
			foreach(KeyValuePair<string, Gtk.TreePath> pair in treepaths)
			{
				if(Regex.Matches(pair.Key, "/").Count > depth)
					obsoletepaths.Add(pair.Key);
			}
			
			foreach(string obsoletepath in obsoletepaths)
			{
				treepaths.Remove(obsoletepath);
			}
			
			if(treepaths.ContainsKey(directory))
				treepaths.Remove(directory);
			
			treepaths.Add(directory, treepath);
		}
		
		public Gtk.TreePath GetCachedTreePath(string directory)
		{
			Gtk.TreePath path = null;
			
			if(treepaths.ContainsKey(directory))
			{
				path = treepaths[directory];
			}
			
			return path;
		}
		
		public void RemoveCachedTreePath(string directory)
		{
			treepaths.Remove(directory);
		}
    }
    
    public class CacheEntry
    {
    	private string dir;
    	private DateTime time;
    	private Gtk.ListStore store;
    	
    	public CacheEntry(string directory, Gtk.ListStore store)
    	{
    		this.dir = directory;
    		this.time = DateTime.Now;
    		this.store = store;
    	}
    	
    	#region Properties
    	
    	public string Directory
    	{
    		get {return dir;}
    	}
    	public DateTime Timestamp
    	{
    		get {return time;}
    	}
    	public Gtk.ListStore Store
    	{
    		get {return store;}
    	}
    	
    	#endregion
    }
}
