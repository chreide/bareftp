// FileTreeNode.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using System.Text;
using bareFTP.Protocol;

namespace bareFTP.Gui.FileManager
{
	public class FileTreeNode {
 
 		string filename;
 		long size = -1;
 		DateTime datetime;
 		string user;
 		string group;
 		string rights;
 		bool isDir;
 		bool isLink;
 		string linkName;
 		
    	public FileTreeNode (string filename, long size, DateTime datetime, 
    				string user, string group, string rights, bool isDir, bool isLink, string linkName)
        {
        	
         	this.filename = filename;
        	this.size = size;
        	this.datetime = datetime;
        	this.user = user;
        	this.group = group;
        	this.rights = rights;
        	this.isDir = isDir;
        	this.isLink = isLink;
        	this.linkName = linkName; 
        }
        
        public FileTreeNode(RemoteFile file)
        {
        	this.filename = file.Filename;
        	this.size = file.Size;
        	this.datetime = file.LastModified;
        	this.user = file.Owner;
        	this.group = file.Group;
        	this.rights = file.Permissions;
        	this.isDir = file.IsDir;
        	this.isLink = file.IsLink;
        	this.linkName = file.Linkdest;
        }
        
        public FileTreeNode(string str)
        {
    		this.filename = str;
    		this.isDir = true;
        }
        
 		
        public Gdk.Pixbuf Type { 
        	get {
        		Gdk.Pixbuf buf = null;
        		try
				{
					if(IsLink)
					{
						if(Gtk.IconTheme.Default.HasIcon(Gtk.Stock.GoBack))
							buf = Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.GoForward, 16, 0);
						else
							buf = Gtk.IconTheme.Default.LoadIcon("stock_right", 16, 0);
					}
					
					else if(!IsLink && IsDir) {
	        				buf = Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.Directory, 16, 0);
	        		}
	        		else
	        		{
						if(!Gtk.IconTheme.Default.HasIcon(Gui.IconFinder.FindIconName(filename)))
							buf = Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.File, 16, 0);
						else
							buf = Gtk.IconTheme.Default.LoadIcon(Gui.IconFinder.FindIconName(filename), 16, 0);
	        		}
				}
				catch { buf = null; }
        		return buf;
        	}
        }
        
        public string FileName { get { return filename; } }
        public long Size 
		{
			get { return size; } 
		}
        public DateTime Date {get { return datetime; } }
        public string User {get { return user; } }
        public string Group {get { return group; } }
        public string Rights {get { return rights; } }
        public bool IsDir { get { return isDir; } }
        public bool IsLink { get { return isLink; } }
        public string LinkName { get { return linkName; } }
    }
    
}
