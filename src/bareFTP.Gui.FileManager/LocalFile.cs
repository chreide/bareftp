// LocalFile.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;

namespace bareFTP.Gui.FileManager
{
	
	
	public class LocalFile
	{
		private string path;
		private long size;
		private bool isdir = false;
		private string permissions;
		private string rootdir;
		
		public string Path {
			get {
				return path;
			}
			set {
				path = value;
			}
		}
		
		public bool Isdir {
			get {
				return isdir;
			}
			set {
				isdir = value;
			}
		}
		
		public long Size {
			get {
				return size;
			}
			set {
				size = value;
			}
		}

		public string Permissions {
			get {
				return permissions;
			}
			set {
				permissions = value;
			}
		}

		public string RootDir {
			get {
				return rootdir;
			}
			set {
				rootdir = value;
			}
		}
		
		public LocalFile(string path)
		{
			this.path = path;
		}
		
		public LocalFile(string path, bool isdir, long size)
		{
			this.path = path;
			this.isdir = isdir;
			this.size = size;
		}
	}
}
