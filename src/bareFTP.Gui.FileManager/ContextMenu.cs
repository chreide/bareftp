// ContextMenu.cs
//
//  Copyright (C) 2008-2010 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using Mono.Unix;

namespace bareFTP.Gui.FileManager
{
	
	public class ContextMenu : Gtk.Menu
	{
		public event EventHandler RenameRequested;
		public event EventHandler DeleteRequested;
		public event EventHandler DirOpenRequested;
		public event EventHandler TransferRequested;
		public event EventHandler ChmodRequested;
		public event EventHandler NewDirRequested;
		public event EventHandler RefreshDirRequested;
		public event EventHandler SendCommandRequested;
		
		private Gtk.ImageMenuItem m0;
		private FileListActionArgs args;
		
		public ContextMenu(bareFTP.Protocol.ProtocolType ptype, int filecount) : base()
		{
			
			//Gtk.AccelGroup agr = new Gtk.AccelGroup();
			//agr.Lock();
			
			m0 = new Gtk.ImageMenuItem(Catalog.GetString("Open Directory"));
			m0.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.Open, 16, 0));
			
			m0.Activated += delegate (object sender, EventArgs e) { DirOpenRequested.Invoke(this, e);};
			this.Append(m0);
			if(filecount == 0)
				m0.Sensitive = false;
			
			Gtk.MenuItem m4 = new Gtk.MenuItem (Catalog.GetString("New Directory"));
			m4.Activated += delegate (object sender, EventArgs e) { NewDirRequested.Invoke ( this, e ); };
			this.Append(m4);
			
			this.Append(new Gtk.SeparatorMenuItem());
			
			Gtk.MenuItem m4a = new Gtk.MenuItem (Catalog.GetString("Refresh"));
			//m4a.AddAccelerator("activate", agr, new Gtk.AccelKey(
            //Gdk.Key.r, Gdk.ModifierType.ControlMask, Gtk.AccelFlags.Visible));
			
			m4a.Activated += delegate (object sender, EventArgs e) { RefreshDirRequested.Invoke ( this, e ); };
			this.Append(m4a);
			
			this.Append(new Gtk.SeparatorMenuItem());
			
			Gtk.ImageMenuItem m01;
			if(ptype != bareFTP.Protocol.ProtocolType.LOCAL)
			{
				m01 = new Gtk.ImageMenuItem(Catalog.GetString("Download"));
				if(Gtk.IconTheme.Default.HasIcon(Gtk.Stock.GoBack))
					m01.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.GoBack, 16, 0));
				else
					m01.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon("stock_left", 16, 0));
			}
			else
			{
				m01 = new Gtk.ImageMenuItem(Catalog.GetString("Upload"));
				if(Gtk.IconTheme.Default.HasIcon(Gtk.Stock.GoForward))
					m01.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.GoForward, 16, 0));
				else
					m01.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon("stock_right", 16, 0));
			}
				
			m01.Activated += delegate (object sender, EventArgs e) { TransferRequested.Invoke(this, e); };
			this.Append(m01);
			if(filecount == 0)
				m01.Sensitive = false;
			
			Gtk.MenuItem m1 = new Gtk.MenuItem (Catalog.GetString("Rename"));
			//m1.AddAccelerator("activate", agr, new Gtk.AccelKey(
            //Gdk.Key.F2, Gdk.ModifierType.ReleaseMask, Gtk.AccelFlags.Visible));
			m1.Activated += delegate (object sender, EventArgs e) { RenameRequested.Invoke ( this, e ); };
			this.Append(m1);
			if(filecount != 1)
				m1.Sensitive = false;
						
			Gtk.ImageMenuItem m2 = new Gtk.ImageMenuItem(Catalog.GetString("Delete"));
			
			m2.Image = new Gtk.Image(Gtk.IconTheme.Default.LoadIcon(Gtk.Stock.Delete, 16, 0));
			m2.Activated += delegate (object sender, EventArgs e) { DeleteRequested.Invoke( this, e ); };
			//m2.AddAccelerator("activate", agr, new Gtk.AccelKey(
            //Gdk.Key.Delete, Gdk.ModifierType.ReleaseMask, Gtk.AccelFlags.Visible));
			
			this.Append(m2);
			if(filecount == 0)
				m2.Sensitive = false;
			
			Gtk.MenuItem m3 = new Gtk.MenuItem(Catalog.GetString("Permissions"));
			m3.Activated += delegate (object sender, EventArgs e) { ChmodRequested.Invoke ( this, e ); };
			this.Append(m3);
			if(filecount == 0)
				m3.Sensitive = false;
			
			Gtk.MenuItem m5 = new Gtk.MenuItem(Catalog.GetString("Send command"));
			m5.Activated += delegate (object sender, EventArgs e) { 
						if(SendCommandRequested != null)
							SendCommandRequested.Invoke ( this, e ); 
			};
			this.Append(m5);
			
			if(ptype != bareFTP.Protocol.ProtocolType.FTP && ptype != bareFTP.Protocol.ProtocolType.FTPS)
				m5.Sensitive = false;
			
		}
		
		public void DisableDirectoryMethods()
		{
			m0.Sensitive = false;
		}
		
		public FileListActionArgs FileArgs
		{
			get { return this.args; }
			set { args = value; }
		}
		
	}
}
