// FileCopier.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.Generic;
using Gtk;

namespace bareFTP.Gui.FileManager
{
	
	
	public class FileUtils
	{
		
		public static void FileCopier(List<string> sources, string cwd, FileTreeNode node, Gtk.TreeViewDropPosition pos, Gdk.DragAction action)
		{
			Gnome.Vfs.Vfs.Initialize();
			
			foreach(string sourcestr in sources)
			{
				if(string.IsNullOrEmpty(sourcestr.Trim()))
					continue;
				
				Gnome.Vfs.Uri source = new Gnome.Vfs.Uri(sourcestr);
				
				// We handle only local files atm
				if(!source.IsLocal)
					continue;
				
				string dest = cwd;
				
				if(node != null)
				{
					if(node.IsDir && (pos == Gtk.TreeViewDropPosition.IntoOrAfter || pos == Gtk.TreeViewDropPosition.IntoOrBefore))
						dest = System.IO.Path.Combine(dest, node.FileName);
				}
				
				if(action == Gdk.DragAction.Copy || action == Gdk.DragAction.Move)
				{
					Gnome.Vfs.Uri dst = new Gnome.Vfs.Uri(System.IO.Path.Combine(dest, source.GetFileInfo().Name));
					if(dst.Exists)
					{
						if(!bareFTP.Gui.Dialog.Dialogs.QuestionDialog(
						    string.Format(Mono.Unix.Catalog.GetString("{0} already exists. Overwrite?"), Gnome.Vfs.Uri.GetLocalPathFromUri(dst.ToString()))))
						continue;
					}
					try
					{
						Gnome.Vfs.XferOptions options = Gnome.Vfs.XferOptions.Default;
						if(action == Gdk.DragAction.Move)
						{
							options = Gnome.Vfs.XferOptions.Default | Gnome.Vfs.XferOptions.Removesource;
						}
						
						Gnome.Vfs.Xfer.XferUri(source, dst, options,
					                 Gnome.Vfs.XferErrorMode.Query, 
					                 Gnome.Vfs.XferOverwriteMode.Replace, 
					                 delegate {	return 1; });
					}
					catch(Exception ex)
					{
						bareFTP.Gui.Dialog.Dialogs.ErrorDialog(ex.Message);
					}
					
					
				}
			}
			
			Gnome.Vfs.Vfs.Shutdown();
		}
		
		
		public static List<LocalFile> CompleteLocalPaths(List<string> topLevelPaths, string cwd)
		{
			List<LocalFile> paths = new List<LocalFile>();
			
			foreach(string path in topLevelPaths)
			{
				string _path = string.Empty;
				string rootdir = string.Empty;
				// If we have a dir separator, this is probably paths from desktop dnd
				if(path == "..")
					continue;
				
				if(path.IndexOf(System.IO.Path.DirectorySeparatorChar) == 0)
				{
					_path = path;
					rootdir = System.IO.Path.GetDirectoryName(path);
				}
				else
				{
					_path = System.IO.Path.Combine(cwd, path);
					rootdir = cwd;
				}
					
				LocalFile lf = new LocalFile(_path);
				lf.RootDir = rootdir;
				
				if((System.IO.File.GetAttributes(_path) | System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory)
					lf.Isdir = true;
				
				paths.Add(lf);
				
				if(lf.Isdir)
				{
					Traverser(_path, paths, rootdir);
				}
			}
			
			return paths;
		}
		
		private static void Traverser(string path, List<LocalFile> _paths, string rootdir)
		{
			
			foreach(string s in System.IO.Directory.GetFileSystemEntries(path))
			{
				LocalFile lf = new LocalFile(s);
				lf.RootDir = rootdir;
				
				if((System.IO.File.GetAttributes(s) & System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory)
					lf.Isdir = true;
				
				_paths.Add(lf);
				
				if(lf.Isdir)
					Traverser(s, _paths, rootdir);
			}
		}
	}
}
