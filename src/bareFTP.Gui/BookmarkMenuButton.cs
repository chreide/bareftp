
using System;

namespace bareFTP.Gui
{
	public class BookmarkMenuButton : Gtk.MenuToolButton
	{
		
		public BookmarkMenuButton(Gtk.Widget icon, string label) : base(null,null)
		{
			Gtk.HBox hbox = base.Child as Gtk.HBox;
			Gtk.Button button = hbox.Children[0] as Gtk.Button;
			Gtk.ToggleButton togglebutton = hbox.Children[1] as Gtk.ToggleButton;
			hbox.Remove(button);
			
			Gtk.Arrow arr = togglebutton.Child as Gtk.Arrow;
			togglebutton.Remove(arr);
			
			hbox = new Gtk.HBox();
			hbox.PackStart(icon,false,false,2);
			hbox.PackStart(new Gtk.Label(label),false,false,2);
			hbox.Add(arr);
			togglebutton.Add(hbox);
		}
	}
}
