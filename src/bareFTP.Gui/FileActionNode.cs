// FileActionNode.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using bareFTP.Protocol;

namespace bareFTP.Gui
{
	
	[Gtk.TreeNode (ListOnly=true)]
    public class FileActionNode : Gtk.TreeNode {
 
		XferFile file;
 		
 		
    	public FileActionNode (XferFile file)
        {
			this.file = file;
        }
        
 		
        [Gtk.TreeNodeValue (Column=0)]
        public string FileName { get { return file.Path.FileNameDisplay; } }
 
        [Gtk.TreeNodeValue (Column=1)]
        public long LocalSize {
			get { 
				if(file.Direction == TransferDirection.Upload)
					return file.Size;
				else
					return file.Marker;
					
			} 
		}

		[Gtk.TreeNodeValue (Column=2)]
        public long RemoteSize {
			get { 
				if(file.Direction == TransferDirection.Download)
					return file.Size;
				else
					return file.Marker;
					
			} 
		}
        
		[Gtk.TreeNodeValue (Column=3)]
        public string Action {
			get { 
				return Enum.GetName(typeof(FileAction), file.Action); 
			} 
		}
		
		public XferFile File {
			get { 
				return file; 
			} 
		}
        
    }
	
}
