// ActivityProgressBar.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using Gtk;

namespace bareFTP.Gui
{
	
	
	public class ActivityProgressBar : Gtk.ProgressBar
	{
		
		private static System.Timers.Timer pulseTimer;

		public ActivityProgressBar() : base()
		{
			pulseTimer = new System.Timers.Timer();
			pulseTimer.Elapsed += new System.Timers.ElapsedEventHandler(PulseBar);
			pulseTimer.Interval = 100;
		}

		void PulseBar(object sender, System.Timers.ElapsedEventArgs e)
		{
			Gtk.Application.Invoke( delegate {
				this.Pulse();
			});
		}

		public void Start()
		{
			pulseTimer.Enabled = true;
		}

		public void Stop()
		{
			pulseTimer.Enabled = false;
			Gtk.Application.Invoke( delegate {
				this.Fraction = 0.0f;
			});
			
		}

	}
}
