// CellRenderer.cs
//
//  Copyright (C) 2008-2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;

namespace bareFTP.Gui
{
		
	public class MyCellRendererText : Gtk.CellRendererText
    {
    	public MyCellRendererText() : base()
    	{
    		this.Height = 16;
    		this.Yalign = 0.0f;
    		this.Ypad = 0;
    	}
		
		public MyCellRendererText(int width) : base()
    	{
    		this.Height = 16;
    		this.Yalign = 0.0f;
    		this.Ypad = 0;
			this.Width = width;
    	}
    	
    }
    
    public class MyCellRendererPixbuf : Gtk.CellRendererPixbuf
    {
    	public MyCellRendererPixbuf() : base()
    	{
    		this.Height = 16;
    	}
    }
	
    public class MyCellRendererProgress : Gtk.CellRendererProgress
    {
    	public MyCellRendererProgress(int width) : base()
    	{
			this.Height = 10;
    		this.Width = width;
			this.Yalign = 0.5f;
			
    	}
    	
    	public MyCellRendererProgress() : base()
    	{
    		this.Height = 16;
    	}
    }
}

/*
// CellRenderers.cs created with MonoDevelop
// User: chreide at 12:19 AM 12/13/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

namespace bareFTP.Gui
{
	
	
	public class MyCellRendererText : Gtk.CellRendererText
    {
    	public MyCellRendererText(int width) : base()
    	{
    		this.Height = 16;
    		this.Yalign = 0.0f;
    		this.Ypad = 0;
    		this.Width = width;
    	}
    	
    }
    
    public class MyCellRendererPixbuf : Gtk.CellRendererPixbuf
    {
    	public MyCellRendererPixbuf() : base()
    	{
    		this.Height = 16;
    	}
    }
	
    public class MyCellRendererProgress : Gtk.CellRendererProgress
    {
    	public MyCellRendererProgress(int width) : base()
    	{
    		this.Height = 16;
    		this.Width = width;
			
    	}
    	
    	public MyCellRendererProgress() : base()
    	{
    		this.Height = 16;
    	}
    }
}
*/
