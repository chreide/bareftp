// PreferencesDialog.cs
//
//  Copyright (C) 2009 Christian Eide
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.Generic;

using Gtk;

namespace bareFTP.Gui.Preferences
{
	
	public partial class PreferencesDialog : Gtk.Dialog
	{
		Gtk.ComboBox cbProtocol;
		bareFTP.Preferences.Config prefs;
		
		public PreferencesDialog(bareFTP.Preferences.Config conf)
		{
			//this.prefs = conf;
			this.prefs = new bareFTP.Preferences.Config(string.Empty);
			this.Build();
			
			bareFTP.Preferences.PwdBackend pwdBackend = new bareFTP.Preferences.PwdBackend();
			
			combo_remote_charset.AppendText("");
			foreach(System.Text.EncodingInfo i in System.Text.Encoding.GetEncodings())
			{
				try 
				{
					combo_remote_charset.AppendText(i.DisplayName);
				}
				catch {}
			}
			
			combo_remote_charset.Changed += on_remote_charset_changed;
			
			cb_use_gnome_keyring.Sensitive = pwdBackend.HasGnomeKeyring;
			
			cbProtocol = bareFTP.Gui.Common.MakeProtocolComboBox();
			this.boxDefaultProtocol.PackStart(cbProtocol, false, false, 0);
			this.boxDefaultProtocol.ShowAll();
			
			// General
			
			GeneralRemoteCharset = prefs.General_RemoteCharset;
			cb_showhiddenfiles.Active = prefs.General_ShowHiddenFiles;
			btn_message_font.FontName = prefs.GUI_MessageFont;
			cb_preservepermissions.Active = prefs.General_PreserveFilePermissions;
			
			cb_showhiddenfiles.Toggled += on_show_hidden_toggled;
			cb_enablecache.Active = prefs.General_EnableDirectoryCache;
			cb_enablecache.Toggled += on_enable_cache_toggled;
			cb_preservepermissions.Toggled += on_preserve_toggled;
			btn_message_font.FontSet += on_font_set;
			cb_use_gnome_keyring.Active = prefs.General_UseGnomeKeyring;
			cb_use_gnome_keyring.Toggled += on_use_gnome_keyring_toggled;
			cb_simultaneous_transfers.Active = !prefs.General_SimultaneousTransfers;
			cb_simultaneous_transfers.Toggled += cb_simultaneous_transfers_toggled;
			lblnumconnections.Sensitive = spin_num_connections.Sensitive = !cb_simultaneous_transfers.Active;
			
			spin_num_connections.Value = prefs.General_MaxConnections;
			spin_num_connections.Changed += spin_num_connections_changed;
			// Connection
			
			//TreeIter iter;
			//if (ls.GetIterFirst (out iter))
			//	combobox1.SetActiveIter (iter);
			
			cbProtocol.Active = prefs.NetworkDefaultProtocol -1;
			cbProtocol.Changed += on_cb_protocol_changed;
			entryNetworkTimeout.Text = (prefs.NetworkTimeout / 1000).ToString();
			entryNetworkTimeout.Changed += on_entry_timeout_changed;
			
			// FTP
			entryDefaultFTPPort.Text = prefs.FTPDefaultPort.ToString();
			entryDefaultFTPPort.Changed += on_default_ftp_port_changed;

			cb_passive.Active = prefs.FTP_PassiveMode;
			entryEmailAddress.Text = prefs.FTP_EmailAddress;
			
			cb_empty_to_anon.Active = prefs.FTP_EmptyUserAnonymous;
			cb_empty_pass_email.Active = prefs.FTP_EmailAsAnonymousPass;
			
			cb_passive.Toggled += on_cbpassive_toggled;
			cb_empty_to_anon.Toggled += on_cb_empty_to_anon_toggled;
			cb_empty_pass_email.Toggled += on_cb_empty_pass_email_toggled; 
			entryEmailAddress.Changed += on_email_changed;
			
			// FTPS
			
			DataChannelProtectionLevel = prefs.FTPSDataChannelProtectionLevel;
			VerifyServerCertificate = prefs.FTPSVerifyServerSertificate;
			
			this.cb_encryptdatachannel.Toggled += on_encryptdatachannel_toggled;
			this.cb_verifyservercert.Toggled += on_verifyservercert_toggled;
			
			// SSH
			
			this.SSHPath = prefs.SSHProgramPath;
			entry_sshPath.Changed += on_sshPath_changed;
			
			entryDefaultSSHPort.Text = prefs.SSH_DefaultPort.ToString();
			entryDefaultSSHPort.Changed += on_default_ssh_port_changed;
			
		}
		
#region General
		
		protected void on_font_set(object sender, EventArgs e)
		{
			prefs.GUI_MessageFont = this.btn_message_font.FontName;
		}
		
		protected void on_remote_charset_changed(object sender, EventArgs e)
		{
			prefs.General_RemoteCharset = GeneralRemoteCharset;
		}
		
		
		protected void on_show_hidden_toggled(object sender, EventArgs e)
		{
			prefs.General_ShowHiddenFiles = cb_showhiddenfiles.Active; 
		}
		protected void on_enable_cache_toggled(object sender, EventArgs e)
		{
			prefs.General_EnableDirectoryCache = cb_enablecache.Active; 
		}
		protected void on_preserve_toggled(object sender, EventArgs e)
		{
			prefs.General_PreserveFilePermissions = cb_preservepermissions.Active;
		}
		
		protected void on_use_gnome_keyring_toggled(object sender, EventArgs e)
		{
			try
			{
				prefs.General_UseGnomeKeyring = cb_use_gnome_keyring.Active;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.ToString());
				bareFTP.Gui.Dialog.Dialogs.ErrorDialog(Mono.Unix.Catalog.GetString("Communication with GnomeKeyring failed and has been disabled"));
				cb_use_gnome_keyring.Active = false;
			}
		}
		
		protected void cb_simultaneous_transfers_toggled(object sender, EventArgs e)
		{
			prefs.General_SimultaneousTransfers = !cb_simultaneous_transfers.Active;
			lblnumconnections.Sensitive = spin_num_connections.Sensitive = !cb_simultaneous_transfers.Active;
		}
		protected void spin_num_connections_changed(object sender, EventArgs e)
		{
			prefs.General_MaxConnections = (int)spin_num_connections.Value;	
		}
		
		public string GeneralRemoteCharset
		{
			get { return combo_remote_charset.ActiveText; }
			
			set 
			{ 
				Gtk.TreeIter iter;
				combo_remote_charset.Model.GetIterFirst(out iter);
				
				do
				{
					if ((string)combo_remote_charset.Model.GetValue(iter,0) == value)
 					break;

				} while (combo_remote_charset.Model.IterNext(ref iter));
				
				combo_remote_charset.SetActiveIter(iter);
			}
		}
		
#endregion
		
#region Connection
		
		
		protected void on_entry_timeout_changed(object sender, EventArgs e)
		{
			int timeout;
			if(Int32.TryParse(entryNetworkTimeout.Text.Trim(), out timeout))
			{
				prefs.NetworkTimeout = timeout * 1000;
			}
			else if(String.IsNullOrEmpty(entryNetworkTimeout.Text.Trim()))
			{
				prefs.NetworkTimeout = -1;
			}
		}
		
		protected void on_cb_protocol_changed(object sender, EventArgs e)
		{
			prefs.NetworkDefaultProtocol = cbProtocol.Active + 1;
		}
		
		
#endregion

#region FTP
		protected void on_cbpassive_toggled(object sender, EventArgs e)
		{
			prefs.FTP_PassiveMode = cb_passive.Active; 		
		}
		
		protected void on_cb_empty_to_anon_toggled(object sender, EventArgs e)
		{
			prefs.FTP_EmptyUserAnonymous = cb_empty_to_anon.Active; 
		}
		
		protected void on_cb_empty_pass_email_toggled(object sender, EventArgs e)
		{
			prefs.FTP_EmailAsAnonymousPass = cb_empty_pass_email.Active; 
		}
		
			
		protected void on_email_changed(object sender, EventArgs e)
		{
			prefs.FTP_EmailAddress = entryEmailAddress.Text; 
		}
		
		protected void on_default_ftp_port_changed(object sender, EventArgs e)
		{
			int port;
			if(Int32.TryParse(entryDefaultFTPPort.Text.Trim(), out port))
				prefs.FTPDefaultPort = port;
			
		}
#endregion

#region FTPS
		
		public void on_encryptdatachannel_toggled (object o, EventArgs args)
		{
			prefs.FTPSDataChannelProtectionLevel = DataChannelProtectionLevel;
		}
		
		public void on_verifyservercert_toggled (object o, EventArgs args)
		{
			prefs.FTPSVerifyServerSertificate = VerifyServerCertificate;
		}

		public string DataChannelProtectionLevel
		{
			get 
			{
				if(this.cb_encryptdatachannel.Active)
					return "P";
				else
					return "C";
			}
			set 
			{ 
				if(value == "P")
					this.cb_encryptdatachannel.Active = true;
				else if(value == "C")
					this.cb_encryptdatachannel.Active = false;
			}
		}
		
		public bool VerifyServerCertificate
		{
			get { return this.cb_verifyservercert.Active; }
			set { this.cb_verifyservercert.Active = prefs.FTPSVerifyServerSertificate; }
		}
#endregion

#region SSH
		
		
		protected void on_sshPath_changed(object sender, EventArgs args)
		{
			prefs.SSHProgramPath = SSHPath;
		}
		
		protected void on_default_ssh_port_changed(object sender, EventArgs args)
		{
			int port;
			if(Int32.TryParse(entryDefaultFTPPort.Text.Trim(), out port))
				prefs.SSH_DefaultPort = port;
		}

		protected virtual void onCloseClicked (object sender, System.EventArgs e)
		{
			prefs.Sync();
			this.Destroy();
			this.Dispose();
		}
		
		public string SSHPath
		{
			get { return this.entry_sshPath.Text; }
			set { this.entry_sshPath.Text = prefs.SSHProgramPath; }
			
		}
		
#endregion
	}
}
